# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
3.times do |topic|
  Topic.create!(
      title: "Topic ##{topic}"
  )
end

puts "3 topics created"


10.times do |idx|
  Blog.create!(
      title: "My Blog Post #{idx}",
      body: "A Lorem Ipsum egy egyszerû szövegrészlete, szövegutánzata a betûszedõ és nyomdaiparnak.
             A Lorem Ipsum az 1500-as évek óta standard szövegrészletként szolgált az iparban; mikor
             egy ismeretlen nyomdász összeállította a betûkészletét és egy példa-könyvet vagy szöveget nyomott
             papírra, ezt használta. Nem csak 5 évszázadot élt túl, de az elektronikus betûkészleteknél is
             változatlanul megmaradt. Az 1960-as években népszerûsítették a Lorem Ipsum részleteket magukbafoglaló Letraset
             lapokkal, és legutóbb softwarekkel mint például az Aldus Pagemaker.",
      topic_id: Topic.last.id
  )
end

puts "10 blogs have been created automatically"

5.times do |idx|
  Skill.create!(
      title: "JavaEE #{idx}",
      percent_utilized: 15 + idx
  )
end

puts "5 skills have been created automatically"

8.times do |idx|
  Portfolio.create!(
      title: "Portfolio #{idx}",
      subtitle: "Ruby on Rails",
      body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
             et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
             laboris nisi ut aliquip ex ea commodo consequat.",
      main_image: "https://placehold.it/600x400",
      thumb_image: "https://placehold.it/350x200"
  )
end

puts "ruby on rails portfolios created"

1.times do |idx|
  Portfolio.create!(
      title: "Portfolio #{idx}",
      subtitle: "Angular",
      body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
             et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
             laboris nisi ut aliquip ex ea commodo consequat.",
      main_image: "https://placehold.it/600x400",
      thumb_image: "https://placehold.it/350x200"
  )
end

puts "1 portfolio item have been created automatically"

3.times do |technology|
  Portfolio.last.technologies.create!(
    name: "Technology #{technology}",
  )
end

puts "3 technologies have been created"